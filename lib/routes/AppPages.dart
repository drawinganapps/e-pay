import 'package:get/get.dart';
import 'package:p_pay/controller/bottom_navigation_binding.dart';
import 'package:p_pay/controller/transaction_history_binding.dart';
import 'package:p_pay/screens/home_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.DASHBOARD,
        page: () => const HomeScreen(),
        bindings: [BottomNavigationBinding(), TransactionHistoryBinding()],
        transition: Transition.rightToLeft)
  ];
}
