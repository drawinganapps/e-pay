import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:p_pay/controller/bottom_navigation.dart';
import 'package:p_pay/pages/dashboard_page.dart';
import 'package:p_pay/pages/statistic_page.dart';
import 'package:p_pay/pages/transaction_history_page.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BottomNavigationController>(
      builder: (controller) {
        return Scaffold(
            body: IndexedStack(
              index: controller.tabIndex,
              children: const [
                DashboardPage(),
                StatisticPage(),
                TransactionHistoryPage()
              ],
            ),
            bottomNavigationBar: Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
                height: 80,
                color: Colors.grey.shade100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                        onPressed: () {
                          controller.changeTabIndex(0);
                        },
                        icon: const Icon(Icons.grid_view_rounded),
                        iconSize: 40,
                        color: controller.tabIndex == 0
                            ? Colors.lightBlue.shade500
                            : Colors.blue.withOpacity(0.2)),
                    IconButton(
                        onPressed: () {
                          controller.changeTabIndex(1);
                        },
                        icon: const Icon(Icons.pie_chart),
                        iconSize: 40,
                        color: controller.tabIndex == 1
                            ? Colors.lightBlue.shade500
                            : Colors.blue.withOpacity(0.2)),
                    IconButton(
                        onPressed: () {
                          controller.changeTabIndex(2);
                        },
                        icon: const Icon(Icons.history),
                        iconSize: 40,
                        color: controller.tabIndex == 2
                            ? Colors.lightBlue.shade500
                            : Colors.blue.withOpacity(0.2)),
                    IconButton(
                        onPressed: () {
                          controller.changeTabIndex(3);
                        },
                        icon: const Icon(Icons.account_circle),
                        iconSize: 40,
                        color: controller.tabIndex == 3
                            ? Colors.lightBlue.shade500
                            : Colors.blue.withOpacity(0.2)),
                  ],
                )));
      },
    );
  }
}
