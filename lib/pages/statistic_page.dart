import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:p_pay/data/dummy.dart';
import 'package:p_pay/model/expenses_chart_model.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class StatisticPage extends StatelessWidget {
  const StatisticPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<ExpensesChartModel> expansesData = Dummy.expansesData;
    return Container(
      padding: const EdgeInsets.only(left: 15, right: 15, top: 30),
      child: Column(
        children: [
          const Center(
            child: Text('Statistic',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.blueGrey)),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('Overview',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.blueGrey)),
              DropdownButton<String>(
                value: 'Yearly',
                icon: const Icon(Icons.expand_more, color: Colors.lightBlue),
                elevation: 16,
                onChanged: (String? newValue) {},
                underline: Container(
                  height: 0,
                ),
                items: <String>['All', 'Weekly', 'Monthly', 'Yearly']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,
                        style: const TextStyle(
                            color: Colors.lightBlue,
                            fontWeight: FontWeight.w500)),
                  );
                }).toList(),
              )
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 5, bottom: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: 140,
                  width: 150,
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(15)),
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(10)),
                        padding: const EdgeInsets.all(5),
                        margin: const EdgeInsets.only(bottom: 10),
                        child: const Icon(
                          Icons.call_received,
                          color: Colors.white,
                          size: 25,
                        ),
                      ),
                      const Text('Income',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 18)),
                      Container(
                        margin: const EdgeInsets.only(bottom: 10),
                      ),
                      const Text('\$354,280',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w900,
                              fontSize: 25)),
                    ],
                  ),
                ),
                Container(
                  height: 140,
                  width: 150,
                  decoration: BoxDecoration(
                      color: Colors.lightBlueAccent,
                      borderRadius: BorderRadius.circular(15)),
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(10)),
                        padding: const EdgeInsets.all(5),
                        margin: const EdgeInsets.only(bottom: 10),
                        child: const Icon(
                          Icons.call_made,
                          color: Colors.white,
                          size: 25,
                        ),
                      ),
                      const Text('Expenses',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 18)),
                      Container(
                        margin: const EdgeInsets.only(bottom: 10),
                      ),
                      const Text('\$126,450',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w900,
                              fontSize: 25)),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 240,
            child: SfCartesianChart(
              margin: const EdgeInsets.all(0),
              borderColor: Colors.white,
              primaryYAxis: NumericAxis(
                minimum: 0,
                maximum: 140,
                interval: 10,
                majorGridLines: const MajorGridLines(width: 1, dashArray: [5]),
              ),
              primaryXAxis: CategoryAxis(
                axisLine: const AxisLine(width: 0),
                labelPosition: ChartDataLabelPosition.outside,
                majorTickLines: const MajorTickLines(width: 0),
                majorGridLines: const MajorGridLines(width: 0),
              ),
              series: <ChartSeries<ExpensesChartModel, String>>[
                ColumnSeries<ExpensesChartModel, String>(
                    dataSource: expansesData,
                    spacing: 0.2,
                    xValueMapper: (ExpensesChartModel data, _) => data.month,
                    yValueMapper: (ExpensesChartModel data, _) => data.y,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(5),
                        topRight: Radius.circular(5)),
                    width: 0.6,
                    color: Colors.blue),
                ColumnSeries<ExpensesChartModel, String>(
                    dataSource: expansesData,
                    spacing: 0.2,
                    xValueMapper: (ExpensesChartModel data, _) => data.month,
                    yValueMapper: (ExpensesChartModel data, _) => data.y1,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(5),
                        topRight: Radius.circular(5)),
                    width: 0.6,
                    color: Colors.lightBlueAccent),
              ],
            ),
          ),
          Container(
            padding:
                const EdgeInsets.only(right: 50, left: 50, top: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(5)),
                      height: 15,
                      width: 15,
                      margin: const EdgeInsets.only(right: 10),
                    ),
                    const Text('Income',
                        style: TextStyle(
                            color: Colors.blueGrey,
                            fontSize: 16,
                            fontWeight: FontWeight.w500))
                  ],
                ),
                Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.lightBlueAccent,
                          borderRadius: BorderRadius.circular(5)),
                      height: 15,
                      width: 15,
                      margin: const EdgeInsets.only(right: 10),
                    ),
                    const Text('Expenses',
                        style: TextStyle(
                            color: Colors.blueGrey,
                            fontSize: 16,
                            fontWeight: FontWeight.w500))
                  ],
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomRight,
                    transform: const GradientRotation(120),
                    colors: [
                      Colors.red.withOpacity(0.8),
                      Colors.redAccent.withOpacity(0.8)
                    ]),
                borderRadius: BorderRadius.circular(15)),
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(top: 15, bottom: 10),
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 15),
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                    child: CircularPercentIndicator(
                      radius: 25.0,
                      lineWidth: 5.0,
                      animation: true,
                      percent: 0.81,
                      center: const Text(
                        "81%",
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 16.0,
                            color: Colors.white),
                      ),
                      circularStrokeCap: CircularStrokeCap.butt,
                      progressColor: Colors.white,
                      backgroundColor: Colors.white.withOpacity(0.3),
                    ),
                  ),
                ),
                const Flexible(
                  child: Text(
                      'Your average income has decreased from last year',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Colors.white)),
                ),
                const Icon(
                  Icons.navigate_next,
                  color: Colors.white,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
