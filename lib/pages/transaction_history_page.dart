import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:p_pay/controller/transaction_history.dart';
import 'package:p_pay/data/dummy.dart';
import 'package:p_pay/model/transaction_model.dart';
import 'package:p_pay/widgets/transaction_hitory_list.dart';

class TransactionHistoryPage extends StatelessWidget {
  const TransactionHistoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<TransactionModel> lastTransactions = Dummy.lastTransaction
        .where((element) => element.transactionCategory == 'Entertain')
        .toList();
    final List<TransactionModel> lastTransferTransactions = Dummy
        .lastTransaction
        .where((element) => element.transactionCategory == 'Transfer')
        .toList();
    final List<TransactionModel> lastBillTransactions = Dummy.lastTransaction
        .where((element) => element.transactionCategory == 'Bill')
        .toList();

    return GetBuilder<TransactionHistoryController>(builder: (controller) {
      return Container(
        padding: const EdgeInsets.only(left: 15, right: 15, top: 30),
        child: Column(
          children: [
            const Center(
              child: Text('Transaction History',
                  style: TextStyle(
                      color: Colors.blueGrey,
                      fontWeight: FontWeight.bold,
                      fontSize: 20)),
            ),
            Container(
              margin: const EdgeInsets.only(top: 10, bottom: 15),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text('Category',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.blueGrey)),
                DropdownButton<String>(
                  value: 'This Week',
                  icon: const Icon(Icons.expand_more, color: Colors.lightBlue),
                  elevation: 16,
                  onChanged: (String? newValue) {},
                  underline: Container(
                    height: 0,
                  ),
                  items: <String>['All', 'This Week', 'This Month', 'This Year']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value,
                          style: const TextStyle(
                              color: Colors.lightBlue,
                              fontWeight: FontWeight.w500)),
                    );
                  }).toList(),
                )
              ],
            ),
            Expanded(
              child: ListView(children: [
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey.withOpacity(0.2)),
                      borderRadius: BorderRadius.circular(10)),
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.orangeAccent,
                                    borderRadius: BorderRadius.circular(50)),
                                padding: const EdgeInsets.all(7),
                                child: const Icon(
                                  Icons.send,
                                  color: Colors.white,
                                  size: 20,
                                ),
                                margin: const EdgeInsets.only(right: 10),
                              ),
                              const Text('Transfer',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blueGrey))
                            ],
                          ),
                          IconButton(
                            onPressed: () {
                              controller.changeTransferCard(
                                  !controller.transferCardExpanded);
                            },
                            icon: Icon(
                              controller.transferCardExpanded
                                  ? Icons.expand_less
                                  : Icons.expand_more,
                              color: Colors.grey,
                            ),
                          )
                        ],
                      ),
                      controller.transferCardExpanded
                          ? TransactionHistoryListWidget(
                              transactions: lastTransferTransactions)
                          : Container(),
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey.withOpacity(0.2)),
                      borderRadius: BorderRadius.circular(10)),
                  padding: const EdgeInsets.all(15),
                  margin: const EdgeInsets.only(top: 15, bottom: 15),
                  child: Column(

                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.red.withOpacity(0.8),
                                    borderRadius: BorderRadius.circular(50)),
                                padding: const EdgeInsets.all(7),
                                child: const Icon(
                                  Icons.play_circle,
                                  color: Colors.white,
                                  size: 20,
                                ),
                                margin: const EdgeInsets.only(right: 10),
                              ),
                              const Text('Entertain',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blueGrey))
                            ],
                          ),
                          IconButton(
                            onPressed: () {
                              controller.changeEntertainCard(
                                  !controller.entertainCardExpanded);
                            },
                            icon: Icon(
                                controller.entertainCardExpanded
                                    ? Icons.expand_less
                                    : Icons.expand_more,
                                color: Colors.grey),
                          )
                        ],
                      ),
                      controller.entertainCardExpanded
                          ? TransactionHistoryListWidget(
                          transactions: lastTransactions)
                          : Container(),
                    ],
                  ),
                ),
                Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey.withOpacity(0.2)),
                        borderRadius: BorderRadius.circular(10)),
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(50)),
                                  padding: const EdgeInsets.all(7),
                                  child: const Icon(
                                    Icons.payments,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                  margin: const EdgeInsets.only(right: 10),
                                ),
                                const Text('Bill',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.blueGrey))
                              ],
                            ),
                            IconButton(
                              onPressed: () {
                                controller.changeBillCard(
                                    !controller.billCardExpanded);
                              },
                              icon: Icon(
                                  controller.billCardExpanded
                                      ? Icons.expand_less
                                      : Icons.expand_more,
                                  color: Colors.grey),
                            )
                          ],
                        ),
                        controller.billCardExpanded
                            ? TransactionHistoryListWidget(
                            transactions: lastBillTransactions)
                            : Container(),
                      ],
                    )),
              ]),
            )
          ],
        ),
      );
    });
  }
}
