import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:p_pay/data/dummy.dart';
import 'package:p_pay/model/transaction_model.dart';
import 'package:p_pay/widgets/transaction_history_widget.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<TransactionModel> lastTransactions = Dummy.lastTransaction;
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              transform: GradientRotation(90),
              colors: [
            Colors.lightGreen,
            Colors.blue,
          ])),
      child: Column(
        children: [
          Container(
            padding:
                const EdgeInsets.only(top: 50, bottom: 10, left: 30, right: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(5),
                      margin: const EdgeInsets.only(right: 15),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(50),
                      ),
                      clipBehavior: Clip.antiAlias,
                      child: Image.asset('assets/img/man.png',
                          width: 45, height: 45),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text('Hello',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w500)),
                        Text('Ahmad Mirlan',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: Colors.white)),
                      ],
                    )
                  ],
                ),
                Badge(
                  position: const BadgePosition(top: 3, end: 3),
                  badgeColor: Colors.pinkAccent,
                  child: const Icon(
                    Icons.notifications_outlined,
                    size: 30,
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20),
            child: Column(
              children: [
                const Text('Balance',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w500)),
                Container(
                  margin: const EdgeInsets.only(top: 10),
                ),
                const Text('\$26,520.00',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 40,
                      letterSpacing: 1.5,
                    )),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 30, bottom: 40),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Container(
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.3),
                            borderRadius: BorderRadius.circular(15)),
                        margin: const EdgeInsets.only(bottom: 10),
                        child: IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.send,
                            color: Colors.white,
                          ),
                        )),
                    const Text('Send',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 15))
                  ],
                ),
                Column(
                  children: [
                    Container(
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.3),
                            borderRadius: BorderRadius.circular(15)),
                        margin: const EdgeInsets.only(bottom: 10),
                        child: IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.add_card,
                            color: Colors.white,
                          ),
                        )),
                    const Text('Top Up',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 15))
                  ],
                ),
                Column(
                  children: [
                    Container(
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.3),
                            borderRadius: BorderRadius.circular(15)),
                        margin: const EdgeInsets.only(bottom: 10),
                        child: IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.payments,
                            color: Colors.white,
                          ),
                        )),
                    const Text('Pay',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 15))
                  ],
                ),
                Column(
                  children: [
                    Container(
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.3),
                            borderRadius: BorderRadius.circular(15)),
                        margin: const EdgeInsets.only(bottom: 10),
                        child: IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.dashboard_customize_rounded,
                            color: Colors.white,
                          ),
                        )),
                    const Text('More',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 15))
                  ],
                ),
              ],
            ),
          ),
          Flexible(
            child: Container(
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(45),
                    topRight: Radius.circular(45),
                  )),
              padding: const EdgeInsets.only(left: 25, right: 25, top: 25),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Text('Last Transaction',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w700)),
                      Text('See all',
                          style: TextStyle(
                              color: Colors.lightBlue,
                              fontWeight: FontWeight.w700))
                    ],
                  ),
                  Flexible(
                    child: ListView(
                      children: List.generate(lastTransactions.length, (index) {
                        return TransactionHistoryWidget(transaction: lastTransactions[index]);
                      }),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
