import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:p_pay/model/transaction_model.dart';

class TransactionHistoryWidget extends StatelessWidget {
  final TransactionModel transaction;
  const TransactionHistoryWidget({Key? key, required this.transaction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                decoration: BoxDecoration(
                    color: transaction.color.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(15)),
                margin: const EdgeInsets.only(right: 20),
                padding: const EdgeInsets.all(10),
                child: Image.asset(
                  'assets/img/${transaction.platformIcon}',
                  height: 35,
                  width: 35,
                ),
              ),
              SizedBox(
                height: 40,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(transaction.platformName,
                        style: const TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 20,
                            color: Colors.blueGrey)),
                    Text(transaction.date,
                        style: const TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.grey)),
                  ],
                ),
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text((transaction.transactionStatus == 0 ? '-' : '+') + '\$${transaction.amount}',
                  style: TextStyle(
                      color: transaction.transactionStatus == 0 ? Colors.red : Colors.green,
                      fontWeight: FontWeight.bold,
                      fontSize: 20)),
              Text(transaction.transactionType,
                  style: const TextStyle(
                      color: Colors.grey, fontWeight: FontWeight.w500))
            ],
          ),
        ],
      ),
    );
  }
}
