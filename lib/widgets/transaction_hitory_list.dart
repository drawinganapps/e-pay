import 'package:flutter/cupertino.dart';
import 'package:p_pay/model/transaction_model.dart';
import 'package:p_pay/widgets/empty_transaction_data.dart';
import 'package:p_pay/widgets/transaction_history_widget.dart';

class TransactionHistoryListWidget extends StatelessWidget {
  final List<TransactionModel> transactions;
  const TransactionHistoryListWidget({Key? key, required this.transactions}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (transactions.isNotEmpty) {
      return Container(
        margin: const EdgeInsets.only(top: 10),
        child: Column(
          children: List.generate(transactions.length, (index) {
            return TransactionHistoryWidget(
                transaction: transactions[index]);
          }),
        ),
      );
    }
    return const EmptyTransactionData();
  }

}
