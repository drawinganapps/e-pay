import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmptyTransactionData extends StatelessWidget {
  const EmptyTransactionData({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('There is no data', style: TextStyle(
      fontWeight: FontWeight.w600,
      fontStyle: FontStyle.italic,
      fontSize: 18,
      color: Colors.grey.withOpacity(0.8)
    ));
  }
}
