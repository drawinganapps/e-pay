class ExpensesChartModel {
  ExpensesChartModel(this.month, this.x, this.y, this.y1);

  final String month;
  final int x;
  final double y;
  final double y1;
}
