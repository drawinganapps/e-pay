import 'package:flutter/cupertino.dart';

class TransactionModel {
  final String date;
  final int transactionStatus;
  final String platformName;
  final String platformIcon;
  final double amount;
  final String transactionCategory;
  final String transactionType;
  final Color color;

  TransactionModel(this.date, this.transactionStatus, this.platformName, this.platformIcon, this.amount, this.transactionCategory, this.transactionType, this.color);
}
