import 'package:get/get.dart';

class TransactionHistoryController extends GetxController {
  var transferCardExpanded = false;
  var entertainCardExpanded = false;
  var billCardExpanded = false;

  void changeTransferCard(bool isExpanded) {
    transferCardExpanded = isExpanded;
    update();
  }

  void changeEntertainCard(bool isExpanded) {
    entertainCardExpanded = isExpanded;
    update();
  }

  void changeBillCard(bool isExpanded) {
    billCardExpanded = isExpanded;
    update();
  }
}
