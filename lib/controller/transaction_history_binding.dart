import 'package:get/get.dart';
import 'package:p_pay/controller/transaction_history.dart';

class TransactionHistoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TransactionHistoryController>(
        () => TransactionHistoryController());
  }
}
