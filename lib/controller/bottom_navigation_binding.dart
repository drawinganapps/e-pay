import 'package:get/get.dart';
import 'package:p_pay/controller/bottom_navigation.dart';

class BottomNavigationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BottomNavigationController>(() => BottomNavigationController());
  }
}
