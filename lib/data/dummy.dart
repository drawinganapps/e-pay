import 'package:flutter/material.dart';
import 'package:p_pay/model/expenses_chart_model.dart';
import 'package:p_pay/model/transaction_model.dart';

class Dummy {
  static List<TransactionModel> lastTransaction = [
    TransactionModel('29 April', 0, 'Netflix', 'netflix.png', 4.99, 'Entertain',
        'Subscription', Colors.red),
    TransactionModel('27 April', 1, 'Paypal', 'paypal.png', 160.5, 'Transfer',
        'Withdraw', Colors.lightBlue),
    TransactionModel('26 April', 0, 'Spotify', 'spotify.png', 2.99, 'Entertain',
        'Subscription', Colors.green),
    TransactionModel('26 April', 0, 'Steam', 'steam.png', 2.99, 'Entertain',
        'Subscription', Colors.black),
    TransactionModel('26 April', 0, 'Discord', 'discord.png', 2.99, 'Entertain',
        'Subscription', Colors.blueAccent),
  ];

  static List<ExpensesChartModel> expansesData = [
    ExpensesChartModel('Jan', 1, 40, 50),
    ExpensesChartModel('Feb', 2, 88, 70),
    ExpensesChartModel('Mar', 3, 39, 60),
    ExpensesChartModel('Apr', 4, 90, 67),
    ExpensesChartModel('May', 5, 85, 89),
  ];
}
